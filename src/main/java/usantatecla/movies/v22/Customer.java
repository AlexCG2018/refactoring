package usantatecla.movies.v22;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class Customer {

	private String name;
	
	private List<Rental> rentals;

	public Customer(String name) {
		this.name = name;
		rentals = new ArrayList<Rental>();
	}

	public void addRental(Rental rental) {
		rentals.add(rental);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		String result = "Rental Record for " + this.getName() + "\n";

		result += getTitles();
		result += "Amount owed is " + String.valueOf(getTotalCharge()) + "\n";
		result += "You earned " + String.valueOf(getTotalFrequentRenterPoints()) + " frequent renter points";

		return result;
	}

	private String getTitles() {
		AtomicReference<String> result = new AtomicReference<>("");
		forEachRental(rental -> result.updateAndGet(v -> v += "\t" + rental.getMovie().getTitle() + "\t" + String.valueOf(rental.getCharge()) + "\n"));
		return result.get();
	}

	private double getTotalCharge() {
		AtomicReference<Double> total = new AtomicReference<>(0.0);
		forEachRental(rental -> total.updateAndGet(v -> (double) (v + rental.getCharge())));
		return total.get();
	}

	private int getTotalFrequentRenterPoints() {
		AtomicInteger total = new AtomicInteger();
		forEachRental(rental -> total.addAndGet(rental.getFrequentRenterPoints()));
		return total.get();
	}

	private void forEachRental(Consumer<Rental> rentalFunction) {
		for (Rental rental : this.rentals) {
			rentalFunction.accept(rental);
		}
	}
}
